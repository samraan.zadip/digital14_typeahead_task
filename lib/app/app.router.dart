// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedRouterGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs, unused_import, non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../core/models/events.dart';
import '../ui/views/detail/detail_view.dart';
import '../ui/views/seatgeek_list/seatgeek_list_view.dart';
import '../ui/views/startup/startup_view.dart';

class Routes {
  static const String startUpView = '/start-up-view';
  static const String seatGeekListView = '/seat-geek-list-view';
  static const String detailView = '/detail-view';
  static const all = <String>{
    startUpView,
    seatGeekListView,
    detailView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.startUpView, page: StartUpView),
    RouteDef(Routes.seatGeekListView, page: SeatGeekListView),
    RouteDef(Routes.detailView, page: DetailView),
  ];
  @override
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    StartUpView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => StartUpView(),
        settings: data,
      );
    },
    SeatGeekListView: (data) {
      return MaterialPageRoute<dynamic>(
        builder: (context) => SeatGeekListView(),
        settings: data,
      );
    },
    DetailView: (data) {
      var args = data.getArgs<DetailViewArguments>(nullOk: false);
      return MaterialPageRoute<dynamic>(
        builder: (context) => DetailView(
          args.event,
          key: args.key,
        ),
        settings: data,
      );
    },
  };
}

/// ************************************************************************
/// Arguments holder classes
/// *************************************************************************

/// DetailView arguments holder class
class DetailViewArguments {
  final Event? event;
  final Key? key;
  DetailViewArguments({required this.event, this.key});
}

/// ************************************************************************
/// Extension for strongly typed navigation
/// *************************************************************************

extension NavigatorStateExtension on NavigationService {
  Future<dynamic> navigateToStartUpView({
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return navigateTo(
      Routes.startUpView,
      id: routerId,
      preventDuplicates: preventDuplicates,
      parameters: parameters,
      transition: transition,
    );
  }

  Future<dynamic> navigateToSeatGeekListView({
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return navigateTo(
      Routes.seatGeekListView,
      id: routerId,
      preventDuplicates: preventDuplicates,
      parameters: parameters,
      transition: transition,
    );
  }

  Future<dynamic> navigateToDetailView({
    required Event? event,
    Key? key,
    int? routerId,
    bool preventDuplicates = true,
    Map<String, String>? parameters,
    Widget Function(BuildContext, Animation<double>, Animation<double>, Widget)?
        transition,
  }) async {
    return navigateTo(
      Routes.detailView,
      arguments: DetailViewArguments(event: event, key: key),
      id: routerId,
      preventDuplicates: preventDuplicates,
      parameters: parameters,
      transition: transition,
    );
  }
}
