// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// StackedLocatorGenerator
// **************************************************************************

// ignore_for_file: public_member_api_docs

import 'package:stacked_core/stacked_core.dart';
import 'package:stacked_services/stacked_services.dart';

import '../core/services/api.dart';
import '../core/services/hive_db_service.dart';
import '../ui/views/detail/detail_viewmodel.dart';
import '../ui/views/seatgeek_list/seatgeek_list_viewmodel.dart';
import '../ui/views/startup/startup_viewmodel.dart';

final locator = StackedLocator.instance;

Future<void> setupLocator(
    {String? environment, EnvironmentFilter? environmentFilter}) async {
// Register environments
  locator.registerEnvironment(
      environment: environment, environmentFilter: environmentFilter);

// Register dependencies
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => SnackbarService());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => HiveDbService());
  locator.registerFactory(() => SeatGeekListViewModel());
  locator.registerFactory(() => StartUpViewModel());
  locator.registerFactory(() => DetailViewModel());
}
