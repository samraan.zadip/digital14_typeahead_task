import 'package:digital14_typeahead_task/ui/views/detail/detail_view.dart';
import 'package:digital14_typeahead_task/ui/views/detail/detail_viewmodel.dart';
import 'package:stacked/stacked_annotations.dart';
import 'package:stacked_services/stacked_services.dart';
import '../core/services/api.dart';
import '../core/services/hive_db_service.dart';
import '../ui/views/seatgeek_list/seatgeek_list_view.dart';
import '../ui/views/seatgeek_list/seatgeek_list_viewmodel.dart';
import '../ui/views/startup/startup_view.dart';
import '../ui/views/startup/startup_viewmodel.dart';

//flutter pub run build_runner build --delete-conflicting-outputs
@StackedApp(logger: StackedLogger(), routes: [
  MaterialRoute(page: StartUpView),
  MaterialRoute(page: SeatGeekListView),
  MaterialRoute(page: DetailView),
], dependencies: [
  //Services
  LazySingleton(classType: NavigationService),
  LazySingleton(classType: SnackbarService),
  LazySingleton(classType: Api),
  LazySingleton(classType: HiveDbService),


  //ViewModels
  Factory(classType: SeatGeekListViewModel),
  Factory(classType: StartUpViewModel),
  Factory(classType: DetailViewModel),
])
class AppSetup {
  /** Serves no purpose besides having an annotation attached to it */
}
