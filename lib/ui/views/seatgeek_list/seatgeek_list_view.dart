import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:digital14_typeahead_task/app/app.router.dart';
import 'package:digital14_typeahead_task/ui/views/favourite_btn/favourite_btn_view.dart';
import 'package:digital14_typeahead_task/ui/views/seatgeek_list/seatgeek_list_viewmodel.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_hooks/stacked_hooks.dart';
import 'package:stacked_services/stacked_services.dart';
import '../../../app/app.locator.dart';
import '../../shared/app_colors.dart';
import '../../shared/e_progress_indicator.dart';
import '../../shared/e_text.dart';
import '../../shared/text_styles.dart';
import 'package:intl/intl.dart';

class SeatGeekListView extends StatelessWidget {
  const SeatGeekListView();

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<SeatGeekListViewModel>.reactive(
        viewModelBuilder: () => locator<SeatGeekListViewModel>(),
        builder: (context, model, child) => Scaffold(
              body: model.isBusy
                  ? const EProgressIndicator()
                  : (model.hasError
                      ? Center(
                          child: EText.body(
                            model.modelError.toString(),
                            color: Colors.redAccent.shade700,
                          ),
                        )
                      : Column(
                          children: [
                            const SearchBar(),
                            Expanded(
                              child: (model.result.isNotEmpty &&
                                      model.showDelete)
                                  ? ListView.separated(
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) =>
                                          DisplayCard(index),
                                      itemCount: model.result.length,
                                      separatorBuilder:
                                          (BuildContext context, int index) =>
                                              const Divider(
                                        color: nGreyColor,
                                      ),
                                    )
                                  : ListView.separated(
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) =>
                                          DisplayCard(index),
                                      itemCount:
                                          model.data?.events?.length ?? 0,
                                      separatorBuilder:
                                          (BuildContext context, int index) =>
                                              const Divider(
                                        color: nGreyColor,
                                      ),
                                    ),
                            )
                          ],
                        )),
            ));
  }
}

class SearchBar extends HookViewModelWidget<SeatGeekListViewModel> {
  const SearchBar() : super(reactive: true);

  @override
  Widget buildViewModelWidget(
      BuildContext context, SeatGeekListViewModel viewModel) {
    var controller = useTextEditingController();
    return Container(
      padding: const EdgeInsets.all(12),
      decoration: const BoxDecoration(color: nPrimaryColor1),
      child: SafeArea(
        child: Row(
          children: [
            Expanded(
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.white24),
                child: TextField(
                  cursorColor: Colors.white,
                  decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: 'Search',
                      hintStyle: bodyStyle.copyWith(color: Colors.white54)),
                  controller: controller,
                  style: bodyStyle.copyWith(color: Colors.white),
                  onChanged: viewModel.onChanged,
                ),
              ),
            ),
            Visibility(
              visible: viewModel.showDelete,
              child: IconButton(
                  onPressed: () {
                    controller.clear();
                    viewModel.onDeletePressed();
                  },
                  splashColor: Colors.white24,
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                  )),
            )
          ],
        ),
      ),
    );
  }
}

class DisplayCard extends ViewModelWidget<SeatGeekListViewModel> {
  final int _index;

  const DisplayCard(this._index) : super(reactive: true);

  @override
  Widget build(BuildContext context, SeatGeekListViewModel viewModel) {
    var event = viewModel.result.isNotEmpty
        ? viewModel.result[_index]
        : viewModel.data!.events![_index];
    return ListTile(
      contentPadding: const EdgeInsets.all(8),
      onTap: () {
        viewModel.onTap(event);
      },
      leading: Stack(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: CachedNetworkImage(
              imageUrl: event.performers?[0].image ?? "placeholder_url",
              placeholder: (context, url) => const CircularProgressIndicator(),
              errorWidget: (context, url, error) => const Icon(Icons.error),
            ),
          ),
          Positioned(top: -11, left: -11, child: FavouriteBtnView(event,visibility: false,))
          // Align(
          //   alignment: Alignment.topLeft,
          //   child: FavouriteBtnView(event),
          // )
        ],
      ),
      title: EText.headingFour(
        event.title ?? "",
        height: 1.2,
      ),
      subtitle: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          EText.caption("${event.venue?.displayLocation}"),
          EText.caption(DateFormat.yMMMEd()
              .add_jm()
              .format(DateTime.parse(event.datetimeLocal ?? ""))),
        ],
      ),
    );
  }
}
