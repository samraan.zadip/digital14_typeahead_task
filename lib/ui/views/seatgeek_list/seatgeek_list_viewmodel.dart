import 'dart:developer';
import 'package:digital14_typeahead_task/app/app.router.dart';
import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.locator.dart';
import '../../../core/models/events.dart';
import '../../../core/services/api.dart';

class SeatGeekListViewModel extends FutureViewModel<Events> {
  final _api = locator<Api>();
  final _navService = locator<NavigationService>();

  List<Event> _result = [];

  bool _showDelete = false;

  bool get showDelete => _showDelete;

  Future<Events> getData() async {
    return _api.fetchList();
  }

  @override
  Future<Events> futureToRun() => getData();

  @override
  void onError(error) {
    log(error.toString(), name: runtimeType.toString());
  }

  void onChanged(String value) {
    log("onChanged", name: runtimeType.toString());
    _result.clear();
    if (value.isEmpty) {
      _showDelete = false;
      _result.clear();
    } else {
      log("value not empty", name: runtimeType.toString());
      _showDelete = true;
      data?.events?.forEach((event) {
        if (event.title!.toLowerCase().contains(value.toLowerCase())) {
          log(event.title!.toLowerCase()+"------"+value.toLowerCase());
          _result.add(event);
        }
      });
      _result.forEach((element) {log("LIST ITEM - "+(element.title??""),name: runtimeType.toString());});
    }
    notifyListeners();
  }

  List<Event> get result => _result;

  void onDeletePressed() {
    _showDelete = false;
    _result.clear();
    notifyListeners();
  }

  onTap(Event event) async {
      await _navService.navigateTo(Routes.detailView,
          arguments: DetailViewArguments(event: event));
      initialise();
      _result.clear();
  }


  goBack() {
    _navService.back();
  }
}
