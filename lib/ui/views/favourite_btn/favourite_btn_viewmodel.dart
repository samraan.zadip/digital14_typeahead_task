import 'package:digital14_typeahead_task/core/services/hive_db_service.dart';
import 'package:stacked/stacked.dart';

import '../../../app/app.locator.dart';

class FavouriteBtnViewModel extends FutureViewModel<bool> {
  final _hiveDbService = locator<HiveDbService>();

  final int _id;

  FavouriteBtnViewModel(this._id);

  @override
  Future<bool> futureToRun() => getData();

  void onPressed() async {
    var box = await _hiveDbService.favouriteBox;
    var doesContain = box.containsKey(_id);
    if (doesContain) {
      box.delete(_id);
    } else {
      box.put(_id, _id);
    }
    initialise();
  }

  Future<bool> getData() async {
    var box = await _hiveDbService.favouriteBox;
    return box.containsKey(_id);
  }
}
