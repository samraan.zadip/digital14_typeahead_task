import 'package:digital14_typeahead_task/ui/shared/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import '../../../core/models/events.dart' as ev;
import 'favourite_btn_viewmodel.dart';

class FavouriteBtnView extends StatelessWidget {
  final ev.Event? _event;
  final bool visibility;

  const FavouriteBtnView(this._event, {Key? key, required this.visibility})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FavouriteBtnViewModel>.reactive(
      viewModelBuilder: () => FavouriteBtnViewModel(_event!.id ?? 0),
      builder: (context, model, child) => model.isBusy
          ? const SizedBox()
          : Visibility(
              visible: (model.data ?? false) || visibility,
              child: IconButton(
                onPressed: () {
                  model.onPressed();
                },
                icon: Icon(
                  model.data! ? Icons.favorite : Icons.favorite_border,
                  color: model.data! ? Colors.redAccent.shade700 : nGreyColor,
                ),
              ),
            ),
    );
  }
}
