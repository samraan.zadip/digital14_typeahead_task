import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';

import '../../../app/app.locator.dart';

class DetailViewModel extends BaseViewModel {
  final _navService = locator<NavigationService>();

  void goBack() {
    _navService.back();
  }
}
