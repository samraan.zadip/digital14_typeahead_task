import 'package:cached_network_image/cached_network_image.dart';
import 'package:digital14_typeahead_task/ui/shared/app_colors.dart';
import 'package:digital14_typeahead_task/ui/views/detail/detail_viewmodel.dart';
import 'package:digital14_typeahead_task/ui/views/favourite_btn/favourite_btn_view.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:stacked/stacked.dart';
import '../../../app/app.locator.dart';
import '../../../core/models/events.dart';
import '../../shared/e_text.dart';

class DetailView extends StatelessWidget {
  final Event? _event;

  const DetailView(this._event, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ViewModelBuilder<DetailViewModel>.nonReactive(
        viewModelBuilder: () => locator<DetailViewModel>(),
        builder: (context, model, child) => SafeArea(
              child: Scaffold(
                backgroundColor: nBackgroundColor,
                body: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            IconButton(
                              onPressed: () {
                                model.goBack();
                              },
                              icon: const Icon(
                                Icons.arrow_back_ios,
                                color: nPrimaryColor1,
                              ),
                            ),
                            Expanded(
                              child: EText.headingThree(
                                _event?.title ?? "",
                                height: 1.2,
                              ),
                            ),
                            FavouriteBtnView(_event,visibility: true,)
                          ],
                        ),
                        const Divider(color: nGreyColor,height: 40,),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(12),
                          child: CachedNetworkImage(
                            imageUrl:
                                _event?.performers?[0].image ?? "placeholder_url",
                            height: size.height * 0.3,
                            width: double.infinity,
                            fit: BoxFit.fill,
                            placeholder: (context, url) =>
                                const CircularProgressIndicator(),
                            errorWidget: (context, url, error) =>
                                const Icon(Icons.error),
                          ),
                        ),
                        const SizedBox(height: 10,),
                        EText.headingThree(DateFormat.yMMMEd().add_jm().format(
                          DateTime.parse(_event?.datetimeLocal ?? ""),
                        )),
                        EText.body(
                          "${_event?.venue?.displayLocation}",
                          fontWeight: FontWeight.w600,
                          height: 1.2,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ));
  }
}
