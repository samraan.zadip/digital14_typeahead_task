import 'package:stacked/stacked.dart';
import 'package:stacked_services/stacked_services.dart';
import '../../../app/app.locator.dart';
import '../../../app/app.router.dart';
import '../../../core/services/hive_db_service.dart';

class StartUpViewModel extends BaseViewModel {
  final _navService = locator<NavigationService>();
  final _hiveDbService = locator<HiveDbService>();


  Future handleStartUpLogic() async {
    await _hiveDbService.initializeHive();
    _navService.clearStackAndShow(Routes.seatGeekListView);
  }
}
