import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:stacked/stacked.dart';

import '../../../app/app.locator.dart';
import '../../shared/app_colors.dart';
import '../../shared/e_text.dart';
import 'startup_viewmodel.dart';

class StartUpView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: const SystemUiOverlayStyle(statusBarColor: Colors.transparent),
      child: ViewModelBuilder<StartUpViewModel>.nonReactive(
        onModelReady: (model) => model.handleStartUpLogic(),
        builder: (context, model, child) => Scaffold(
          backgroundColor: nBackgroundColor,
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              EText.body("Digital14 - Coding Task by Samraan",color: nBlackColor,),
              Center(
                child: SpinKitPouringHourGlassRefined(
                  color: nPrimaryColor1,
                  size: size.height * 0.06,
                  strokeWidth: 1.4,
                ),
              )
            ],
          ),
        ),
        viewModelBuilder: () => locator<StartUpViewModel>(),
      ),
    );
  }
}
