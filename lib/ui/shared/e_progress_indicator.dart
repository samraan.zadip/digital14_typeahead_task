
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'app_colors.dart';

class EProgressIndicator extends StatelessWidget {
  final bool alignCenter;
  final Color color;

  const EProgressIndicator(
      {this.alignCenter = true, this.color = nPrimaryColor1});

  @override
  Widget build(BuildContext context) {
    if (alignCenter) {
      return Center(
        child: SpinKitFadingGrid(
          color: color,
          size: MediaQuery.of(context).size.height * 0.064,
        ),
      );
    } else {
      return SpinKitFadingGrid(
        color: color,
        size: MediaQuery.of(context).size.height * 0.064,
      );
    }
  }
}
