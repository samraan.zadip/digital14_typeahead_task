
import 'package:flutter/material.dart';

import 'app_colors.dart';
import 'text_styles.dart';

class EText extends StatelessWidget {
  final String text;
  final TextStyle style;
  final TextAlign textAlign;

  EText.headingOne(this.text, {double? height, textAlign = TextAlign.start})
      : style = heading1Style.copyWith(height: height),
        textAlign = textAlign;

  EText.headingTwo(this.text, {height, textAlign = TextAlign.start})
      : style = heading2Style.copyWith(height: height),
        textAlign = textAlign;

  EText.headingThree(this.text, {double? height, textAlign = TextAlign.start})
      : style = heading3Style.copyWith(height: height),
        textAlign = textAlign;

  EText.headingFour(this.text, {double? height, textAlign = TextAlign.start})
      : style = heading4Style.copyWith(height: height),
        textAlign = textAlign;

  EText.headline(this.text, {double? height, textAlign = TextAlign.start})
      : style = headlineStyle.copyWith(height: height),
        textAlign = textAlign;

  EText.subheading(this.text, {height, textAlign = TextAlign.start})
      : style = subheadingStyle.copyWith(height: height),
        textAlign = textAlign;

  EText.caption(this.text,
      {double? height, Color? color, textAlign = TextAlign.start,FontWeight? fontWeight})
      : style = captionStyle.copyWith(height: height, color: color,fontWeight: fontWeight),
        textAlign = textAlign;

  EText.body(this.text,
      {Color color = nGreyColor,
      double? height,
      FontWeight? fontWeight,
      textAlign = TextAlign.start})
      : style = bodyStyle.copyWith(
            color: color, height: height, fontWeight: fontWeight),
        textAlign = textAlign;

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: style,
      textAlign: textAlign,
    );
  }
}
