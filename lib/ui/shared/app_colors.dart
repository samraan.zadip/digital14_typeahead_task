import 'package:flutter/material.dart';


const Color nLightPrimaryColor = Color(0xffe6e8ed);
const Color nBlackColor = Color(0xff110413);
const Color nLightBlackColor = Color(0xff585a65);
const Color nGreyColor = Color(0xffa0a2ab);
const Color nLightGreyColor = Color(0xffe6e8ed);
const Color nBackgroundColor = Color(0xffeef1f4); //use as card color
const Color nCardColor = Colors.white;
// const Color sdfs = Color(0xff07f5b2);
const Color nPrimaryColor2 = Color(0xff35b2b5);
const Color nPrimaryColor1 = Color(0xff892cbf);
const Color nPrimaryColor1Vibrant = Color(0xff970bea);
const Color nFillColor = Color(0xfff6f6f6);
const Color kcRedColor = Color(0xfff44336);
const Color kcOrangeColor = Color(0xffff9800);




