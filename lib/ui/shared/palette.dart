//palette.dart
import 'package:flutter/material.dart';
class Palette {
  static const MaterialColor primaryColorPalette = MaterialColor(_primaryValue, <int, Color>{
    50: Color(0xFFF1E6F7),
    100: Color(0xFFDCC0EC),
    200: Color(0xFFC496DF),
    300: Color(0xFFAC6BD2),
    400: Color(0xFF9B4CC9),
    500: Color(_primaryValue),
    600: Color(0xFF8127B9),
    700: Color(0xFF7621B1),
    800: Color(0xFF6C1BA9),
    900: Color(0xFF59109B),
  });
  static const int _primaryValue = 0xFF892CBF;

  static const MaterialColor mcgpalette0Accent = MaterialColor(_mcgpalette0AccentValue, <int, Color>{
    100: Color(0xFFE5CCFF),
    200: Color(_mcgpalette0AccentValue),
    400: Color(0xFFB066FF),
    700: Color(0xFFA34DFF),
  });
  static const int _mcgpalette0AccentValue = 0xFFCA99FF;
}