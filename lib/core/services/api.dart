import 'dart:developer';
import 'package:http/http.dart' as http;

import '../models/events.dart';

const _clientID = "MjgwMDU5MTR8MTY1ODU4NjczMi4wNTYxNTk";

class Api {
  var client = http.Client();

  Future<Events> fetchList() async {
    final response = await _makeGetRequest(
        "https://api.seatgeek.com/2/events?client_id=$_clientID&q=Texas+Rangers");
    log("${response.request?.url.toString()}\n${response.body}",
        name: runtimeType.toString());

    if (response.statusCode == 200) {
      var events = eventsFromJson(response.body);
      String s = "";
      return events;
    }
    throw ("something went wrong");
  }

  //
  // Future<http.Response> _makeDirectoryPostRequest(
  //     String directory, Map<String, dynamic> body,
  //     {Map<String, String>? headers}) async {
  //   var response = await client.post(Uri.parse(endpoint + directory),
  //       body: body,
  //       headers: headers ??
  //           const {
  //             "Accept": "application/json",
  //             "Content-Type": "application/x-www-form-urlencoded"
  //           },
  //       encoding: Encoding.getByName("utf-8"));
  //   return response;
  // }

  // Future<http.Response> _makeUrlPostRequest(
  //     String url, Map<String, dynamic> body,
  //     {Map<String, String>? headers, bool jsonEncodeBody = false}) async {
  //   var response = await client.post(Uri.parse(url),
  //       body: jsonEncodeBody ? jsonEncode(body) : body,
  //       headers: headers ??
  //           const {
  //             "Accept": "application/json",
  //             "Content-Type": "application/x-www-form-urlencoded",
  //             "Authorization": ""
  //           },
  //       encoding: Encoding.getByName("utf-8"));
  //   return response;
  // }

  Future<http.Response> _makeGetRequest(String url,
      {Map<String, String>? headers}) async {
    var response = await client.get(
      Uri.parse(url),
      headers: headers ??
          const {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded"
          },
    );
    log(response.toString(), name: runtimeType.toString());
    return response;
  }
}
