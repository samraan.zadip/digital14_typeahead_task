
import 'package:hive_flutter/hive_flutter.dart';

const String _favouriteBox = "favouriteBox";
class HiveDbService {
  initializeHive() async {
    await Hive.initFlutter();
    await Hive.openBox<int>(_favouriteBox);
  }



  Future<Box<int>> get favouriteBox =>  Hive.openBox<int>(_favouriteBox);

}
