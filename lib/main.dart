import 'package:flutter/material.dart';

import 'app/app.locator.dart';
import 'app/app.router.dart';
import 'ui/shared/app_colors.dart';
import 'ui/shared/palette.dart';
import 'package:stacked_services/stacked_services.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'digital14_typeahead_task',
      debugShowCheckedModeBanner: false,
      navigatorKey: StackedService.navigatorKey,
      onGenerateRoute: StackedRouter().onGenerateRoute,
      locale: const Locale("en_US"),
      initialRoute: Routes.startUpView,
      theme: ThemeData(
        primarySwatch: Palette.primaryColorPalette,
        fontFamily: 'Cairo',
        backgroundColor: nBackgroundColor,
        cardColor: nCardColor,
      ),
      // home: StartUpView(),
    );
  }
}
